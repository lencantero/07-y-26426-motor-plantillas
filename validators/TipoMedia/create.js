const Joi = require("joi");

const tipoMediaStoreSchema = Joi.object({
    nombre: Joi.string()
    .pattern(/^[a-zA-Z]+$/)
    .min(4)
    .required()
    .messages({
        'string.base': 'El nombre debe ser una cadena de texto.',
        'string.empty': 'El nombre no puede estar vacío.',
        'string.pattern.base': 'El nombre solo debe contener letras.',
        'string.min': 'El nombre debe tener al menos 4 caracteres.',
        'any.required': 'El nombre es un campo obligatorio.'
    }),

    activo: Joi.boolean()
    .required()
    .messages({
        'any.required': 'El campo activo es obligatorio',
        'boolean.base': 'El campo activo debe ser verdadero o falso'
    })


});

module.exports = tipoMediaStoreSchema

