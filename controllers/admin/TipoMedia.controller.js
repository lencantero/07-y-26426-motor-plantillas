const express = require("express");
const {db} = require("../../db/conexion");
const tipoMediaStoreSchema = require("../../validators/TipoMedia/create");
const tipoMediaEditSchema = require("../../validators/TipoMedia/edit");



const TipoMediaController = {
    // index listado de /admin/media
    index: async function(req, res) {
        console.log("Filtros de búsqueda", req.query);

        let query = "SELECT * FROM TipoMedia WHERE activo = 1";
        const params = [];

        if (req.query['s']) {
            for (const prop in req.query['s']) {
                if (req.query['s'][prop]) {
                    console.log("prop", prop, req.query['s'][prop]);
                    if (prop === 'nombre' || prop === 'orden') {
                        query += ` AND ${prop} LIKE ?`;
                        params.push(`%${req.query['s'][prop]}%`);
                    } else {
                        query += ` AND ${prop} = ?`;
                        params.push(req.query['s'][prop]);
                    }
                }
            }
        }

        db.all(query, params, (err, rows) => {
            if (err) {
                console.error("Error al listar TipoMedia:", err.message);
                res.status(500).send("Error al listar TipoMedia");
                return;
            }

            console.log(rows);
            res.render('admin/TipoMedia/index', {
                tipo_media: rows,
                filtros: req.query['s'] || {}
            });
        });
    },


    create: async function (req, res) {
        const mensaje = req.query.mensaje;
        res.render("admin/TipoMedia/crearForm", {
            mensaje: mensaje
        });
    },

    store: async function (req, res) {
        const { error, value } = tipoMediaStoreSchema.validate(req.body, { abortEarly: false });
        if (error) {
            const mensaje = error.details.map(detail => detail.message).join('/');
            return res.redirect(`/admin/TipoMedia/crear?mensaje=${encodeURIComponent(mensaje)}`);
        }

        db.get(
            "SELECT MAX(orden) + 1 AS orden FROM TipoMedia",
            [],
            (err, resp) => {
                if (err) {
                    console.log("error", err);
                } else {
                    db.run(
                        "INSERT INTO TipoMedia(nombre, orden, activo) VALUES (?, ?, ?)",
                        [
                            value.nombre,
                            resp.orden,
                            value.activo,
                        ],
                        (err) => {
                            if (err) {
                                console.log("error", err);
                            } else {
                                res.redirect("/admin/TipoMedia/listar");
                            }
                        }
                    );
                }
            }
        );
    },

    update: function (req, res) {
        const { error, value } = tipoMediaEditSchema.validate(req.body, { abortEarly: false });
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);
        console.log("En la edicion", req.body);
        if (error) {
            const mensaje = error.details.map(detail => detail.message).join('; ');
            return res.redirect(`/admin/TipoMedia/crear?mensaje=${encodeURIComponent(mensaje)}`);
          }
        db.run(
            "UPDATE TipoMedia SET nombre = ? activo = ? WHERE id_tipo_media = ?",
            [
                value.nombre,
                value.activo,
                idTipoMedia,
            ],
            (err) => {
                if (err) {
                    console.error("Error al eliminar el Tipo de Media:", err);
                }
                res.redirect('/admin/TipoMedia/listar?mesagge=¡El Tipo Media se ha actualizado!');
            }
        );
    },

    edit: function (req, res) {
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);

        db.get(
            "SELECT * FROM TipoMedia WHERE id_tipo_media = ?",
            [idTipoMedia],
            (err, TipoMedia) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los integrantes", err.message);
                    return res.status(500).send("Error interno del servidor");
                }

                if (!TipoMedia) {
                    return res.status(404).send("Integrante no encontrado");
                }

                res.render("admin/TipoMedia/editForm", {
                    TipoMedia: TipoMedia,
                });
            }
        );
    },

    destroy: function (req, res) {
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);
        db.run(
            "UPDATE TipoMedia SET activo = 0 WHERE id_tipo_media = ?",
            [idTipoMedia],
            (err) => {
                if (err) {
                    console.error("Error al eliminar el Tipo Media:", err);
                    return res.redirect('/admin/TipoMedia/listar?mensagge=Error al eliminar el Tipo Media');
                }
                console.log(`Tipo Media con ID ${idTipoMedia} eliminado.`);
                return res.redirect('/admin/TipoMedia/listar?mensagge=El Tipo Media fue eliminado exitosamente');
            }
        );
    }
};



module.exports = TipoMediaController;