const express = require("express");
const {db} = require("../../db/conexion");
const { EMPTY } = require("sqlite3");


const fs = require("fs");
const multer = require("multer");
const mediaStoreSchema = require("../../validators/Media/create");
const mediaEditSchema = require("../../validators/Media/edit");
const upload = multer({ dest: "../../public/assets/images/" });
const fileUpload = upload.single("url_imagen");


// index listado de /admin/media
const MediaController = {
    index: async function (req, res) {
        try {
            let query = 'SELECT m.*, i.nombre AS integranteNombre FROM media m LEFT JOIN integrantes i ON m.matricula = i.matricula WHERE m.activo = 1';
            const params = [];

            console.log("Filtros de búsqueda", req.query);
            if (req.query['s']) {
                for (const prop in req.query['s']) {
                    if (req.query['s'][prop]) {
                        console.log("prop", prop, req.query['s'][prop]);
                        if (prop === 'id_tipo_media') {
                            query += ` AND m.${prop} LIKE ?`;
                            params.push(`%${req.query['s'][prop]}%`);
                        } else if (prop === 'nombre') {
                            query += ` AND i.${prop} LIKE ?`;
                            params.push(`%${req.query['s'][prop]}%`);
                        } else if (prop === 'id') {
                            query += ` AND m.${prop} = ?`;
                            params.push(req.query['s'][prop]);
                        } else {
                            query += ` AND m.${prop} = ?`;
                            params.push(req.query['s'][prop]);
                        }
                    }
                }
            }

            db.all(query, params, (err, rows) => {
                if (err) {
                    console.error('Error al listar media:', err.message);
                    res.status(500).send('Error al listar media');
                    return;
                }
                console.log('Media rows:', rows);
                res.render('admin/media/index', {
                    media: rows,
                });
            });
        } catch (error) {
            console.error('Error al listar media:', error);
            res.status(500).send('Error al listar media');
        }
    },


    create: async function (req, res){
        const sql = "SELECT matricula FROM integrantes WHERE activo = 1";
        const mensaje = req.query.mensaje;
        db.all(sql, (err, matricula) => {
            if (err) {
                console.error(err.message);
                res.status(500).send("Error interno del servidor");
                return;
            }
    
            res.render("admin/Media/crearForm", {
                mensaje: mensaje,
                matricula: matricula
            });
        });
    },

    store: async function(req, res) {
        try {
              
            
            const { error, value } = mediaStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const mensaje = encodeURIComponent("El campo matricula no puede estar vacío. Por favor verifique");
                return res.redirect(`/admin/Media/crear?mensaje=${mensaje}`);
            }
    
            // Obtiene el siguiente valor de 'orden'
            const getOrderQuery = "SELECT MAX(orden) + 1 AS orden FROM Media";
            db.get(getOrderQuery, [], (err, resp) => {
                if (err) {
                    console.error("Error al obtener el siguiente orden", err);
                    return res.status(500).send("Error al obtener el siguiente orden");
                }
    
                // Valor predeterminado de 1 si esta es la primera entrada
                const nextOrden = resp.orden || 1;
                const imageUrl = req.file ? `/assets/images/${req.file.filename}` : '';
    
                // Inserta la nueva entrada de medios
                const insertQuery = `
                    INSERT INTO Media(titulo, nombre, url_video, url_imagen, matricula, id_tipo_media, orden)
                    VALUES (?, ?, ?, ?, ?, ?, ?)
                `;
                db.run(insertQuery, [
                    value.titulo,
                    value.nombre,
                    value.url_video,
                    imageUrl,
                    value.matricula,
                    value.id_tipo_media,
                    nextOrden,
                ], (err) => {
                    if (err) {
                        console.error("Error al insertar la entrada de medios", err);
                        return res.status(500).send("Error al insertar la entrada de medios");
                    } else {
                        res.redirect("/admin/Media/listar");
                    }
                });
            });
        } catch (error) {
            console.error("Error inesperado", error);
            res.status(500).send("Error inesperado");
        }
    },
    
    
    update: function(req, res) {
        const { error, value } = mediaEditSchema.validate(req.body);
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        console.log("En la edicion", req.body);
        
        if (error) {
            const mensaje = encodeURIComponent(error.details.map(detail => detail.message).join('; '));
            const params = new URLSearchParams({
                mensaje,
                titulo: req.body.titulo,
                nombre: req.body.nombre,
                url_video: req.body.url_video,
                matricula: req.body.matricula
            });
            return res.redirect(`/admin/Media/edit/${idMedia}?${params.toString()}`);
        }
    
        const imageUrl = req.file ? `public/assets/images/${req.file.filename}` : value.url_imagen;
        
        db.run(
            "UPDATE Media SET titulo = ?, nombre = ?, url_video =?, url_imagen = ?, matricula = ? WHERE id = ?",
            [
                value.titulo,
                value.nombre,
                value.url_video,
                imageUrl,
                value.matricula,
                idMedia,
            ],
            (err) => {
                if (err) {
                    console.error("Error al actualizar el dato Media:", err);
                    return res.status(500).send("Error interno del servidor");
                }
                res.redirect('/admin/Media/listar');
            }
        );
    },

     edit(req, res) {
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        
        db.get(
            "SELECT * FROM Media WHERE id = ?",
            [idMedia],
            (err, Media) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los datos de Media", err.message);
                    return res.status(500).send("Error interno del servidor");
                }
    
                if (!Media) {
                    return res.status(404).send("Datos de Media no encontrados");
                }
    
                res.render("admin/Media/editForm", {
                    Media: Media,
                });
            }
        );
    },
    destroy(req, res) {
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        db.run(
            "UPDATE Media SET activo = 0 WHERE id = ?",
            [idMedia],
            (err) => {
                if (err) {
                    console.error("Error al eliminar el Media:", err);
                    return res.redirect('/admin/Media/listar?mensagge=Error al eliminar el Media');
                }
                console.log(`Tipo Media con ID ${idMedia} eliminado.`);
                return res.redirect('/admin/Media/listar?mensagge=La Media fue eliminada exitosamente');
            }
        )
    },
 
}


module.exports = MediaController;