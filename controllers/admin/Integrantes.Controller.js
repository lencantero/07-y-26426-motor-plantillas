const express = require("express");
const {db} = require("../../db/conexion");
const IntegrantesModel = require("../../models/integrante.model");
const integranteStoreSchema = require("../../validators/integrantes/create");
   
   /*index - listado
      create - formulario de creación
      store - método de guardar en la base de datos
      show - formulario de ver un registor
      update - método de editar un registro
      edit - formulario de edición 
      destroy - operación de eliminar un registro */

    //INDEX LISTADO DE INTEGRANTES en Admin/Integrantes /integrantes/listar'
    const IntegrantesController = {
  
        index: async function (req, res) {
            try {
                const mensaje = req.query.mensaje;
                const integrantes = await IntegrantesModel.getAll(req);
                res.render("admin/integrantes/index", {
                    integrantes: integrantes,
                    mensaje: mensaje
                    });
                } catch (error) {
                    res.render("admin/integrantes/index", {
                        integrantes: [],
                        error: "Error al obtener los integrantes " 
                    });
                }
            },
 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////       
        
        create: async function (req, res) { // Cambiar el orden de los parámetros
            const mensaje = req.query.mensaje;
            res.render("admin/integrantes/crearForm", {
                mensaje: mensaje
            });
        },

        store: async function (req, res) {
            try {
                const { error} = integranteStoreSchema.validate(req.body, { abortEarly: false });
                if (error) {
                    const mensaje = error.details.map(detail => detail.message).join('; ');
                    return res.redirect(`/admin/integrantes/crear?mensaje=${encodeURIComponent(mensaje)}`);
                }
        
                res.redirect("/admin/integrantes/listar");
            } catch (error) {
                console.error("Error al almacenar el integrante:", error);
                res.status(500).send("Error interno del servidor");
            }
        },
        

     show () {},
     
     update: function(req, res) {
        const { error, value } = integrantesEditSchema.validate(req.body);
        const idIntegrante = parseInt(req.params.idIntegrante);
    
        console.log("idIntegrante", idIntegrante);
        console.log("En la edicion", req.body);
    
        if (error) {
            const mensaje = encodeURIComponent(`Error de validación: ${error.details.map(err => err.message).join(', ')}`);
            return res.redirect(`/admin/integrantes/edit/${idIntegrante}?mensaje=${mensaje}`);
        }
    
        db.run(
            "UPDATE integrantes SET matricula = ?, nombre = ?, apellido = ?, url = ?, activo = ? WHERE id = ?",
            [
                value.matricula,
                value.nombre,
                value.apellido,
                value.url,  
                value.activo,
                idIntegrante,
            ],
            (err) => {
                if (err) {
                    console.error("Error al actualizar el integrante:", err);
                    return res.status(500).send("Error al actualizar el integrante");
                }
                res.redirect('/admin/integrantes/listar?mensaje=¡El integrante se ha actualizado!');
            }
        );
    },

     edit(req, res) {
        const idIntegrante = parseInt(req.params.idIntegrante);
        console.log("idIntegrante", idIntegrante);
        
        db.get(
            "SELECT * FROM integrantes WHERE id = ?",
            [idIntegrante],
            (err, integrante) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los integrantes", err.message);
                    return res.status(500).send("Error interno del servidor");
                }
    
                if (!integrante) {
                    return res.status(404).send("Integrante no encontrado");
                }
    
                res.render("admin/integrantes/editForm", {
                    integrante: integrante,
                });
            }
        );
    },
    
   
    
     //Eliminar registro - la eliminacion no debe de ser fisica de la db
     // si no en el fondo la eliminacion realmente editar un campo
     //que se utiliza en el borrado logico 
     destroy (req, res) {
        const idIntegrante = parseInt(req.params.idIntegrante);
        console.log("idIntegrante", idIntegrante);
        db.run(
            "UPDATE integrantes SET activo = 0 WHERE id = ?",
            [
                idIntegrante
            ],
            (err)=>{
                if(err) {
                    
                    console.error("Error al eliminar el integrante:", err);
                    return res.redirect('/admin/integrantes/listar?mensaje2=Error al eliminar el integrante');

                }
                console.log(`Integrante con ID ${idIntegrante} eliminado.`);
                res.redirect('/admin/integrantes/listar?mensaje2=El fue Integrante eliminado exitosamente');

            }
        );
    },
};
    // Exportar el controlador para que pueda ser utilizado en otras partes de la aplicación
    module.exports = IntegrantesController;

    