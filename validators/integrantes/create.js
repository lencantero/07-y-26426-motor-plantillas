const Joi = require("joi");

const integranteStoreSchema = Joi.object({
    matricula: Joi.string()
        .alphanum()
        .min(4)
        .required()
        .messages({
            'string.base': 'La matrícula debe ser una cadena de texto.',
            'string.empty': 'La matrícula no puede estar vacía.',
            'string.min': 'La matrícula debe tener al menos 4 caracteres.',
            'any.required': 'La matrícula es un campo obligatorio.'
        }),
    nombre: Joi.string()
        .pattern(/^[a-zA-Z]+$/)
        .min(3)
        .required()
        .messages({
            'string.base': 'El nombre debe ser una cadena de texto.',
            'string.empty': 'El nombre no puede estar vacío.',
            'string.pattern.base': 'El nombre solo debe contener letras.',
            'string.min': 'El nombre debe tener al menos 3 caracteres.',
            'any.required': 'El nombre es un campo obligatorio.'
        }),
    apellido: Joi.string()
        .pattern(/^[a-zA-Z]+$/)
        .min(3)
        .required()
        .messages({
            'string.base': 'El apellido debe ser una cadena de texto.',
            'string.empty': 'El apellido no puede estar vacío.',
            'string.pattern.base': 'El apellido debe contener solo letras.',
            'string.min': 'El apellido debe tener al menos 3 caracteres.',
            'any.required': 'El apellido es un campo obligatorio.'
        }),
        url: Joi.string()
        .min(4)
        .messages({
            'string.base': 'La url debe ser una cadena de texto.',
            'string.min': ' La url debe tener al menos 4 letras.',
            'any.required': 'El apellido es un campo obligatorio.'

        }),

        activo: Joi.required()
        
});
module.exports = integranteStoreSchema;