const Joi = require('joi');

const integrantesEditSchema = Joi.object({
    matricula: Joi.string()
        .alphanum()
        .min(4)
        .max(20)
        .required()
        .messages({
            'string.base': 'La matrícula debe ser una cadena de texto.',
            'string.empty': 'La matrícula no puede estar vacía.',
            'string.min': 'La matrícula debe tener al menos 4 caracteres.',
            'string.max': 'La matrícula no debe exceder 20 caracteres.',
            'any.required': 'La matrícula es un campo obligatorio.'
        }),
    nombre: Joi.string()
        .pattern(/^[a-zA-Z]+$/)
        .min(2)
        .max(45)
        .required()
        .messages({
            'string.base': 'El nombre debe ser una cadena de texto.',
            'string.empty': 'El nombre no puede estar vacío.',
            'string.pattern.base': 'El nombre debe contener solo letras.',
            'string.min': 'El nombre debe tener al menos 2 caracteres.',
            'string.max': 'El nombre no puede tener más de 45 caracteres.',
            'any.required': 'El nombre es un campo obligatorio.'
        }),
    apellido: Joi.string()
        .pattern(/^[a-zA-Z]+$/)
        .min(2)
        .max(45)
        .required()
        .messages({
            'string.base': 'El apellido debe ser una cadena de texto.',
            'string.empty': 'El apellido no puede estar vacío.',
            'string.pattern.base': 'El apellido debe contener solo letras.',
            'string.min': 'El apellido debe tener al menos 2 caracteres.',
            'string.max': 'El apellido no puede tener más de 45 caracteres.',
            'any.required': 'El apellido es un campo obligatorio.'
        }),
    url: Joi.optional(),
    activo: Joi.optional(),
});

module.exports = integrantesEditSchema;