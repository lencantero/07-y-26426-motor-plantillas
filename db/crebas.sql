
CREATE TABLE IF NOT EXISTS "usuarios" (
	"id" INTEGER PRIMARY KEY,
	"email" TEXT,
	"contrasenha" TEXT,
	"sysAdmin" INTEGER,
	"matricula" TEXT,
	FOREIGN KEY ("matricula") REFERENCES "integrantes"("matricula")
);


-- Índices
--CREATE INDEX IF NOT EXISTS idx_tipo_media ON Media (id_tipo_media);
--CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media (matricula);
