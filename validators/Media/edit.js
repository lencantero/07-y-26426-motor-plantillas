const Joi = require("joi");

const mediaEditSchema = Joi.object({
    titulo: Joi.string()
        .required()
        .min(5)
        .messages({
            "string.base": `El titulo debe ser una cadena`,
            "string.empty": `El titulo no puede estar vacio`,
            "string.min": `El titulo debe tener al menos 5 caracteres`,
            "any.required": `El campo tituloes obligatorio`
        }),
    nombre: Joi.optional(),
    id_tipo_media: Joi.required(),

    matricula: Joi.required()
        .messages({
            "string.empty": `El campo matricula no puede estar vacio`,
            "any.required": `El campo matricula es obligatorio`
        }),
    url_video: Joi.optional(),
    url_imagen: Joi.optional(),

    activo: Joi.required()
        .messages({
        "any.required": `El campo activo es obligatorio, seleccione uno`
        })
    
});

module.exports = mediaEditSchema;