const Joi = require("joi");

const mediaStoreSchema = Joi.object({
    titulo: Joi.string()
        .required()
        .min(5)
        .messages({
            "string.base": `"titulo" debe ser una cadena`,
            "string.empty": `"titulo" es obligatorio`,
            "string.min": `"titulo" debe tener al menos 5 caracteres`,
            "any.required": `"titulo" es obligatorio`
        }),
    id_tipo_media: Joi.number()
        .integer()
        .required()
        .messages({
            "number.base": `"Tipo Medio" debe seleccionar un tipo medio`,
            "number.integer": `"Tipo Medio" debe seleccionar un tipo medio`,
            "any.required": `"Tipo Medio" es obligatorio`
        }),
    matricula: Joi.required()
        .messages({
            "string.empty": `"matricula" es obligatorio`,
            "any.required": `"matricula" es obligatorio`
        }),
    url_video: Joi.optional(),
    url_imagen: Joi.optional(),

    activo: Joi.required()
});

module.exports = mediaStoreSchema;