
const {db} = require("../db/conexion"); //direccion 
//get all - obtener los registros - filtros de busquedas opcionales deben estar dispobiles por cada campo de la tabla
const IntegrantesModel = {
    // Obtener todos los registros - filtros opcionales de búsqueda por cada campo de la tabla
    getAll: function(req) {
        let sql = "SELECT * FROM Integrantes WHERE activo = 1";

        // Agregar filtros opcionales por cada campo de la tabla
        if (req.query && req.query.s) {
            for (const prop in req.query["s"]) {
                if (req.query["s"][prop]) {
                    console.log("prop", prop, req.query["s"][prop]);
                    sql += ` AND LOWER(${prop}) = LOWER('${req.query["s"][prop]}')`;
                }
            }
        }
        
        // Devolver una promesa 
        return new Promise((resolve, reject) => {
            db.all(sql, (err, integrantes) => {
                if (err) {
                    reject(err.message);
                }
                resolve(integrantes);
            });
        });
    },
    create: async function(data) {
        return new Promise((resolve, reject) => {
            db.get("SELECT MAX(orden) + 1 AS orden FROM integrantes", [], (err, resp) => {
                if (err) {
                    console.log("error", err);
                    reject("Error interno del servidor");
                } else {
                    const nuevoOrden = resp.orden || 1; // En caso de que no haya ningún registro, empieza desde 1
                    db.run(
                        "INSERT INTO integrantes (matricula, nombre, apellido, url, orden, activo) VALUES (?, ?, ?, ?, ?, ?)",
                        [
                            data.matricula,
                            data.nombre,
                            data.apellido,
                            data.url,
                            nuevoOrden,
                            data.activo,
                        ],
                        (err) => {
                            if (err) {
                                console.log("error", err);
                                reject("Error interno del servidor");
                            } else {
                                resolve("Registro creado exitosamente");
                            }
                        }
                    );
                }
            });
        });
    }
 }
module.exports = IntegrantesModel;


