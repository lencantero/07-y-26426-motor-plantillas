const express = require('express');
const bodyParser= require("body-parser");
const hbs = require('hbs');
const app = express();
require("dotenv").config();
const routerAdmin = require ("./routes/admin");
const session = require('express-session');

// Importamos archivos de rutas
const router = require("./routes/public");
// Usamos los archivos de rutas
app.use(bodyParser.urlencoded({ extended: true}));
app.use("/", router);
app.use("/admin", routerAdmin);

//configuracion para motor de vistas hbs
app.use(express.static('public'));
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
hbs.registerPartials(__dirname + "/views/partials");


app.use(
    session({
        name: 'is3-session-name',
        secret: 'clave-aleatoria-y-secreta',
        resave: false,
        httpOnly: true,
        saveUninitialized: false,
    })
);
//middleware
authControl = (req, res, next) => {
    if (!req.session.logueado) {
        return res.redirect('/login');
    } else {
        next();
    }
};

const puerto = process.env.PORT || 3000;

app.listen(puerto, () => {
    console.log(`El servidor se está ejecutando en http://localhost:${puerto}`);
});


